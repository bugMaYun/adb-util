package com.example.adbtest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ds.util.adblib.ADBResult;
import com.ds.util.adblib.AdbHandler;
import com.ds.util.adblib.result.CommandResponse;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainAcitivity";
    private EditText cmdEt;
    private Button runCmdBt;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cmdEt = findViewById(R.id.edit);
        runCmdBt = findViewById(R.id.button);
        textView = findViewById(R.id.textView);

        runCmdBt.setOnClickListener(view -> {
            runCmd(cmdEt.getText().toString());
        });
    }


    private void runCmd(String str) {
        AdbHandler.getInstance().AsynchronousMultiAdb(str, false, new ADBResult() {
            @Override
            public void onAdbResult(CommandResponse result) {
                textView.post(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(result.toString());
                    }
                });
            }
        });
    }

}