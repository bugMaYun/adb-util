package com.ds.util.adblib.adb;

import com.blankj.utilcode.util.ShellUtils;
import com.blankj.utilcode.util.Utils;

public interface IAdb {
    ShellUtils.CommandResult runCmd(final String command,
                                    final boolean isRooted);

    ShellUtils.CommandResult runCmd(final String[] command,
                                    final boolean isRooted);
}
